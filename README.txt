CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Installation
 * Configuration
 * Maintainers


INTRODUCTION
------------

Do you remember the way the configuration forms work in Drupal 7? No need to
create a submit method to store your variables, so if you miss this in Drupal 8
this module is for you my lazy friend.

 * For a full description of the module, visit the project page:
   https://www.drupal.org/project/lazy_config_form

 * To submit bug reports and feature suggestions, or to track changes:
   https://www.drupal.org/project/issues/search/lazy_config_form


REQUIREMENTS
------------

No special requirements.


INSTALLATION
------------

 * Install as you would normally install a contributed Drupal module.
   See: https://www.drupal.org/docs/8/extending-drupal-8/installing-modules
   for further information.


CONFIGURATION
-------------

The module has no menu or modifiable settings. There is no configuration.


MAINTAINERS
-----------

Current maintainers:
 * Adrian Cid Almaguer (adriancid) - https://www.drupal.org/u/adriancid
